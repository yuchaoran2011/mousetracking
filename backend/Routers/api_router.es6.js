var express = require('express');
var mongoose = require('mongoose');
var Participant = require('model/Participant.es6.js');
var Answer = require('model/Answer.es6.js');
var Movement = require('model/Movement.es6.js');
var moment = require('moment');
var db_config = require('configure.js');
var api_router = express.Router();

mongoose.connect(db_config.getDatabaseUri());

var handleSaveFailure = function(err, res) {
    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
};

api_router.post('/movement', function(req, res) {
    var new_movement = new Movement(req.body);
    new_movement.save(function(err) {
        handleSaveFailure(err, res);
    });
});

api_router.post('/participant', function(req, res) {
    var participant = req.body;
    participant.time = moment().toISOString();
    var new_participant = new Participant(participant);
    new_participant.save(function(err) {
        handleSaveFailure(err, res);
    });
});

api_router.post('/answer', function(req, res) {
    var new_answer = new Answer(req.body);
    new_answer.save(function(err) {
        handleSaveFailure(err, res);
    });
});

module.exports = api_router;
