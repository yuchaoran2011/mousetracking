class SuperCar {
    getAttribute(attribute) {
        if (attribute in this) {
            return this[attribute];
        } else {
            throw 'Can not find attribute ${attribute_name}';
        }
    }

    setVisibility(attribute_name, should_show) {
        if (attribute_name in this.visibility) {
            this.visibility[attribute_name] = should_show;
        } else {
            throw 'Can not find attribute ${attribute_name}';
        }
    };

    shouldShowValue(attribute_name) {
        if (attribute_name in this.visibility) {
            return this.visibility[attribute_name];
        } else {
            throw 'Can not find attribute ${attribute_name}';
        }
    };
}

export default SuperCar;
