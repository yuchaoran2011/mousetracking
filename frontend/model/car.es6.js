import SuperCar from "./supercar.es6";

class Car extends SuperCar {
  constructor(mileage, year, price, resale_price, gas_efficiency, label) {
    super();
    this.mileage = mileage;
    this.year = year;
    this.price = price;
    this.resale_price = resale_price;
    this.gas_efficiency = gas_efficiency;
    this.visibility = {
      'Mileage': false,
      'Year': false,
      'Number of Previous Owners': false,
      'Number of Accidents': false,
      'Number of Reviews': false,
    };
    this.can_show = [false, false, false, false, false];
    this.label = label;
  }

  static attribute_name() {
    console.log('rigou');
    return ['Mileage', 'Age', 'Number of Accidents', 'Number of Previous Owners', 'Months between maintenance'];
  }

  static attribute_name_value_map() {
    return {
      'Year': 'mileage',
      'Mileage': 'year',
      'Number of Previous Owners': 'price',
      'Number of Reviews': 'resale_price',
      'Number of Accidents': 'gas_efficiency'
    };
  }
}

export default Car;
