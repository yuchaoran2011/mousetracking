import Car from './car.es6.js';
import GCar from "./gcar.es6";

class Game {
  constructor(number, carA, carB, script, answer) {
    this.number = number;
    this.carA = carA;
    this.carB = carB;
    this.script = script;
    this.answer = answer;
    this.access_history = {
      A: [false, false, false, false, false],
      B: [false, false, false, false, false]
    };

    this.g_access_history = {
      A: [false, false, false, false],
      B: [false, false, false, false]
    };

    //TODO: Delete after complete refactor
    this.tips = script;
  }

  static fromCSV(number, single_data, script) {
    var carA, carB;
    if (single_data.length === 10) {
      carA = new Car(single_data[0], single_data[1], single_data[2], single_data[3], single_data[4], 'A');
      carB = new Car(single_data[5], single_data[6], single_data[7], single_data[8], single_data[9], 'B');
      return new Game(number, carA, carB, script, single_data[10]);
    } else {
      carA = new GCar(single_data[0], single_data[1], single_data[2], single_data[3], 'A');
      carB = new GCar(single_data[4], single_data[5], single_data[6], single_data[7], 'B');
      return new Game(number, carA, carB, script, single_data[8]);
    }
  }

  shouldShowBorder(car, index){}
}

export default Game;
