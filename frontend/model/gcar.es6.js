import SuperCar from "./supercar.es6";

class GCar extends SuperCar {
    constructor(mileage, year, price, resale_price, label) {
        super();
        this.mileage = mileage;
        this.year = year;
        this.price = price;
        this.resale_price = resale_price;
        this.visibility = {
            'Mileage(0.8)': false,
            'Year(0.7)': false,
            'Number of Previous Owners(0.6)': false,
            'Number of Reviews(0.55)': false,
        };
        this.can_show = [false, false, false, false, false];
        this.label = label;
    }

    static attribute_name() {
        return ['Mileage(0.8)', 'Year(0.7)', 'Number of Previous Owners(0.6)', 'Number of Reviews(0.55)'];
    }

    static attribute_name_value_map() {
        return {
            'Mileage(0.8)': 'mileage',
            'Year(0.7)': 'year',
            'Number of Previous Owners(0.6)': 'price',
            'Number of Reviews(0.55)': 'resale_price',
        };
    }
}

export default GCar;
