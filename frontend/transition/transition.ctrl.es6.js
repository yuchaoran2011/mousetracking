TransitionCtrl.$inject = ['$state'];

function TransitionCtrl($state) {
  var vm = this;
  console.log($state);

  vm.next = function() {
    switch($state.params.strategy) {
      case 'ttb':
        document.documentElement.requestFullscreen();
        $state.go('ttb-game');
        break;
      case 'tally':
        $state.go('tally-game');
        break;
      case 'delta':
        $state.go('delta-game');
        break;
      case 'g':
        $state.go('g-game');
        break;
      default:
        break;
    }
  };
}

export default TransitionCtrl;
