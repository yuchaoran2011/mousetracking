import Car from '../model/car.es6.js';
import TTBDemo from '../resources/demo.data.strategy1.es6.js';
import TallyDemo from '../resources/demo.data.strategy2.es6.js';
import DeltaDemo from '../resources/demo.data.strategy3.es6.js';
import GameData from '../Data.es6.js';

DemoCtrl.$inject = ['$location', '$state', '_'];

function DemoCtrl($location, $state, _) {
  var vm = this;

  vm.prevInst = function() {
    vm.currentStep--;
  };

  vm.nextInst = function() {
    vm.currentStep++;
  };

  vm.nextGame = function() {
    //At the end of the tutorial, move the tutorial complete page
    if (vm.gameNo === vm.games.length - 1) {
      switch($state.current.name) {
        case 'ttb-demo':
          $state.go('transition', {strategy: 'ttb'});
          break;
        case 'tally-demo':
          $state.go('transition', {strategy: 'tally'});
          break;
        case 'delta-demo':
          $state.go('transition', {strategy: 'delta'});
          break;
        default:
          throw 'Unknown demo mode'
      }
    } else {
      vm.gameNo++;
      initGame();
    }
  };

  vm.selectCar = function(carLabel) {
    if (vm.games[vm.gameNo].answer === carLabel) {
      vm.isCorrect = true;
    } else {
      vm.isCorrect = false;
    }

    vm.currentStep = vm.scripts.length -1;
    vm.shouldDisplayAnswer = true;
  };

  vm.shouldShowBorder = function(car, index) {
    if(vm.scripts.length - 1 > vm.currentStep && vm.scripts[vm.currentStep].highlights) {
      let highlights = vm.scripts[vm.currentStep].highlights[car];
      if(_.includes(highlights, index)) {
        return 'cover_block_circled';
      }
    }

    return 'cover_block';
  };

  vm.onMouseOver = function(attribute_name, car, index) {
    if (!vm.demo.shouldShowData(vm.gameNo, vm.currentStep)) {
      return;
    }

    if (vm.demo.validateMovement(vm.games[vm.gameNo], car, index)) {
      vm.games[vm.gameNo].access_history[car.label][index] = true;
      car.setVisibility(attribute_name, true);
    } else {
      alert(vm.demo.validationFailureMsg());
    }
  };

  vm.onMouseLeave = function(attribute_name, car, index) {
    car.setVisibility(attribute_name, false);
  };

  function init() {
    switch($state.current.name) {
      case 'ttb-demo':
        vm.demo = new TTBDemo();
        vm.demo.compare = new GameData().getTTBComparisonData();
        break;
      case 'tally-demo':
        vm.demo = new TallyDemo();
        vm.demo.compare = new GameData().getTallyComparisonData();
        break;
      case 'delta-demo':
        vm.demo = new DeltaDemo();
        vm.demo.compare = new GameData().getDeltaComparisonData();
        break;
      default:
        throw 'Unknown demo mode'
    }

    vm.Car = Car;
    vm.gameNo = 0;
    vm.shouldDisplayAnswer = false;
    vm.games = vm.demo.data;
    initGame();
  }

  function initGame() {
    vm.shouldDisplayAnswer = false;
    vm.currentStep = 0;
    vm.scripts = vm.games[vm.gameNo].script;
    vm.carA = vm.games[vm.gameNo].carA;
    vm.carB = vm.games[vm.gameNo].carB;
  }

  init();
}

export default DemoCtrl;
