import Car from './model/car.es6.js';
import GameData from './Data.es6.js';
import SuperCar from "./model/supercar.es6";
import GCar from "./model/gcar.es6";

GameCtrl.$inject = ['$state', 'GameService', 'localStorageService', 'moment'];

function GameCtrl($state, GameService, localStorageService, moment) {
  var vm = this;
  init();

  var uuid, username;
  vm.lastMoveTime = moment().format('x');

  vm.handleMouseMove = function(event) {
    var dot, eventDoc, doc, body, pageX, pageY;
    var offset = $("#container").offset();

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
          (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }
    var x_coord = event.pageX - offset.left;
    var y_coord = event.pageY - offset.top;
    vm.lastMoveTime = moment().format('x');
    GameService.sendCoord({
      x_coord,
      y_coord,
      uuid : uuid,
      time : moment().toISOString(),
      game_id: vm.gameNo
    });
  };

  vm.onMouseOver = function(attribute_name, car, index) {
    vm.games[vm.gameNo].access_history[car.label][index] = true;
    vm.isInside = true;
    car.setVisibility(attribute_name, true);
  };

  vm.onMouseLeave = function(attribute_name, car) {
    vm.isInside = false;
    car.setVisibility(attribute_name, false);
  };

  vm.userEffortGood = function() {
    var access_history = vm.games[vm.gameNo].access_history;
    var a_checked = false;
    var b_checked = false;
    for (let i = 0 ; i < 5; i++) {
      if (access_history.A[i] === true) {
        a_checked = true
      }
      if (access_history.B[i] === true) {
        b_checked = true
      }
    }

    return a_checked && b_checked;
  };

  vm.selectCar = function(car) {
    if (!vm.userEffortGood()) {
      alert('In order to reach a decision, you need to look at AT LEAST TWO cue values, at least one for System A and at least one for System B. Please try again.');
      return;
    }

    if (vm.currentGame.answer === car) {
      vm.isCorrect = true;
      vm.displayedAnswer = "CORRECT";
    } else {
      vm.isCorrect = false;
      vm.displayedAnswer = "INCORRECT"
    }
    if (car === vm.currentGame.answer) {
      vm.correctNo = vm.correctNo + 1;
    }

    vm.shouldDisplayAnswer = true;

    GameService.sendGame({
      uuid : uuid,
      game_id: vm.gameNo,
      correct_answer: vm.currentGame.answer,
      user_answer: car,
      time: moment().toISOString(),
      game_type: $state.current.name.split('-')[0]
    });

    if (vm.gameNo === vm.games.length - 1) {
      vm.endGame = true;
    }
  };

  vm.selectGame = function(gameNo) {
    vm.gameNo = gameNo;
    vm.currentStep = 0;
    vm.shouldDisplayAnswer = false;
    vm.currentGame = vm.games[vm.gameNo];
    vm.carA = vm.games[vm.gameNo].carA;
    vm.carB = vm.games[vm.gameNo].carB;
    vm.tips = vm.games[vm.gameNo].tips;
    vm.browseHistory_A = [false, false, false, false, false];
    vm.browseHistory_B = [false, false, false, false, false];
    vm.showErrorMsg = false;
  };

  vm.nextInst = function() {
    vm.currentStep++;
  };

  vm.prevInst = function() {
    vm.currentStep--;
  };

  vm.nextGame = function() {
    if (vm.gameNo === vm.games.length - 1) {
      vm.endGame = true;
      return;
    }

    vm.selectGame(vm.gameNo + 1);
  };

  vm.prolific = function() {
    window.location.href = 'https://app.prolific.co/submissions/complete?cc=7F95AA31';
  };

  function init() {
    username = localStorageService.get('username');
    uuid = localStorageService.get('uuid');
    var dataSource = new GameData();
    var order = JSON.parse(localStorageService.get('order'));
    vm.Car = Car;
    vm.isInside = false;
    vm.endGame = false;
    vm.correctNo = 0;
    vm.gameNo = 0;

    switch($state.current.name) {
      case 'g-game':
        vm.games = dataSource.getGData(order);
        vm.compare = dataSource.getEmptyComparisonData();
        vm.Car = GCar;
        break;
      case 'ttb-game':
        vm.games = dataSource.getTTBData(order);
        vm.compare = dataSource.getEmptyComparisonData();
        break;
      case 'tally-game':
        vm.games = dataSource.getTallyData(order);
        vm.compare = dataSource.getEmptyComparisonData();
        break;
      case 'delta-game':
        vm.games = dataSource.getDeltaData(order);
        vm.compare = dataSource.getDeltaComparisonData();
        break;
      default:
        throw 'Can not find matching game data';
    }

    vm.currentGame = vm.games[vm.gameNo];
    vm.carA = vm.games[vm.gameNo].carA;
    vm.carB = vm.games[vm.gameNo].carB;
  }
}

export default GameCtrl;
