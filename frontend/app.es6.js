require('./reset.less');
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import LocalStorageModule from 'angular-local-storage';
import moment from 'moment';
import _ from 'lodash';

import ConsentCtrl from './consent/consent.ctrl.es6.js';
import GameCtrl from './game.ctrl.es6.js';
import DemoCtrl from './demo/demo.ctrl.es6.js';
import TransitionCtrl from './transition/transition.ctrl.es6.js';
import GameService from './game.service.es6.js';
import UUIDService from './uuid.service.es6.js';
import appConfig from './config.es6.js';

angular.module('app', [uiRouter, LocalStorageModule])
  .controller('ConsentCtrl', ConsentCtrl)
  .controller('GameCtrl', GameCtrl)
  .controller('DemoCtrl', DemoCtrl)
  .controller('TransitionCtrl', TransitionCtrl)
  .constant('moment', moment)
  .constant('_', _)
  .factory('GameService', GameService)
  .factory('UUIDService', UUIDService)
  .config(appConfig);
