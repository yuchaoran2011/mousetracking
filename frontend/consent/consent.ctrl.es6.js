import GameData from "../Data.es6";

ConsentCtrl.$inject = ['$state', 'localStorageService', 'UUIDService', 'GameService'];

function ConsentCtrl($state, localStorageService, UUIDService, GameService) {
  var vm = this;

  vm.onAgree = function () {
    if (!vm.username) {
      alert('Please input your full name.');
      return;
    } else {
      var uuid = UUIDService.generateUUID();
      localStorageService.set('username', vm.username);
      localStorageService.set('uuid', uuid);
      var data_source = new GameData();
      var order;
      if ($state.current.name !== 'g') {
        order = data_source.getDataOrder();
      } else {
        order = data_source.getGDataOrder();
      }
      GameService.saveUser({
        name: vm.username,
        uuid,
        game_type: $state.current.name,
        order: order
      });
      localStorageService.set('order', JSON.stringify(order));
    }

    switch($state.current.name) {
      case 'ttb':
        $state.go('transition', {strategy: 'ttb'});
        break;
      case 'tally':
        $state.go('transition', {strategy: 'tally'});
        break;
      case 'delta':
        $state.go('delta-demo');
        break;
      case 'g':
        $state.go('transition', {strategy: 'g'});
        break;
      default:
        break;
    }
  }
}

export default ConsentCtrl;
