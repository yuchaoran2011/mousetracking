import Game from '../model/game.es6.js'

class DemoDataStrategy3 {
  constructor() {
    const B = 'B';
    const A = 'A';

    const data = [
      ['2009','300,000','1','20','1','2015','32,000','1','10','1',B],
      ['2015','35,000','1','20','6','2018','10,000','1','20','6',B],
      ['2010','50,000','1','10','8','2009','45,000','3','10','8',A],
      ['2007','100,000','3','1','8','2002','125,000','3','10','2',A],
      ['2018','8000','1','20','1','2017','10,000','1','50','2',A]
    ];

    const game1_script = [
      {text:'You want to buy a used car. You searched online and found two potential cars. You want to choose the one that has higer value. The table below gives some information to aid your decision.'},
      {text:'Each value in the table can be used to predict the value of the cars. Keep in mind: the fewer years, mileages, number of previous owners, and accident reports the car has, the better value.And more reviews the car has, the higher value.'},
      {text:'There are five cues for each of the two cars. The cues are ranked from top to bottom by their relevance to the car’s value, with year the most relevant and accident report the least.'},
      {text:'You will be using a strategy known as Δ (delta) inference. A threshold Δ is the difference between values of two cues that can differentiate two objects meaningfully. For example, a car with 5000 miles cannot be differentiated from a car with 5010 miles, because the difference, 10, is too small. However, it can be differentiated from a car with 15,000 miles.'},
      {text:'Thresholds Δ of five cues are written in the column between two cars. Δ inference starts from searching for the most important cue: Year in this case. If the difference between year values of two cars exceeds 3, we choose the more recent car. If the difference is less than or equal to Δ, we move on to the second most important cue, i.e. Mileage, and repeat the same procedure.'},
      {text:'In the following examples, you will use Δ inference to decide on the car that has higher value.'},
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value and then click Next.', highlights: {A: [], B: [0]}},
      {text:'Since Car B’s Year is less than Car A’s Year by more than 3.  Car B is our winner! click Choose B'}
    ]; 

    const game2_script = [
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value. Since year difference of A and B is equal to 3 years, we cannot make a decision, we move on to the second most important cue – Mileage. Click next.', highlights: {A: [], B: [0]}},
      {text:'Move the mouse to the second cue: Car A\'s Mileage. Check out the value and then click Next.', highlights: {A: [1], B: []}},
      {text:'Move the mouse to the second cue: Car B\'s Mileage. Check out the value and then click Next.', highlights: {A: [], B: [1]}},
      {text:'Since Car B’s mileage is less than Car A\'s Mileage by more than 10,000 miles, B is our winner! Click Choose B'}
    ];

    const game3_script = [
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value. Since year difference is less than 3, we cannot make a decision, we move on to the second most important cue – Mileage. Click next.', highlights: {A: [], B: [0]}},
      {text:'Move the mouse to the second cue: Car A\'s Mileage. Check out the value and then click Next.', highlights: {A: [1], B: []}},
      {text:'Move the mouse to the second cue: Car B\'s Mileage. Check out the value. Since mileage difference is less than 10,000 miles, we cannot make a decision, we move on to the third most important cue – Number of Owners. Click next.', highlights: {A: [], B: [1]}},
      {text:'Move the mouse to the first cue: Car A\'s Number of owners. Check out the value and then click Next.', highlights: {A: [2], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Number of owners. Check out the value and then click Next.', highlights: {A: [], B: [2]}},
      {text:'Since Car A has fewer owners by more than 1, A is our winner! Click Choose A'}
    ];

    const default_script = [
      {text:'Now please apply Δ inference to the game below'}
    ];

    var game_data = [];
    data.forEach(function(single_data) {
      game_data.push(Game.fromCSV(single_data, default_script));
    });
    game_data[0].script = game1_script;
    game_data[1].script = game2_script;
    game_data[2].script = game3_script;

    this.data = game_data;
  }

  shouldShowData(gameNo, currentStep) {
    if (gameNo === 0 && currentStep <= 5) {
      return false;
    }

    return true;
  }

  validationFailureMsg() {
    return 'Please use Δ inference to open the box.';
  }

  validateMovement(game, car, index) {
    // No restriction with we have instruction
    if (game.script.length > 1) {
      return true;
    }

    var access_history = game.access_history;
    for(let i = 0 ; i < index; i++) {
      if(access_history.A[i] === false || access_history.B[i] === false) {
        return false;
      }
    }
    return true;
  }
}

export default DemoDataStrategy3;
