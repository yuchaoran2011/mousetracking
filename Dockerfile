FROM node:12.16.0

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .
RUN npm install -g webpack@1.15.0 webpack-cli@2.0.10

RUN mkdir -p /data/db

RUN apt-get update &&\
	wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add - &&\
	echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list &&\
	apt-get update &&\
	apt-get install -y curl mongodb-org=4.2.3 mongodb-org-server=4.2.3 mongodb-org-shell=4.2.3 mongodb-org-mongos=4.2.3 mongodb-org-tools=4.2.3
